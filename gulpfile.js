var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var uglifycss = require('gulp-uglifycss');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('concat', function() {
  return gulp.src('pre-js/*.js')
    .pipe(concat('all.js'))
    .pipe(gulp.dest('js'))
});

gulp.task('compress', function() {
  return gulp.src('pre-js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('js'))
});

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'app'
    }
  });
});

gulp.task('sass', function(){
  return gulp.src('app/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

/*gulp.task('serve', gulp.series('sass'), function(){
  browserSync.init({
    server: "./app"
  });
});*/

gulp.task('css', function(){
  gulp.src('./app/css/*.css')
    .pipe(uglifycss({
      "uglyComments": true
    }))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('run', gulp.series('sass', 'browserSync', 'css'));

gulp.task('watch', function(){
  gulp.watch('app/scss/**/*.scss', gulp.series('sass'));
  gulp.watch('app/css/*.css', gulp.series('css'));
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/*.js', browserSync.reload);
});

gulp.task('default', gulp.series('run', 'browserSync', 'watch'));

gulp.task('useref', function(){
  return gulp.src('app/*.html')
    .pipe(useref())
    .pipe(gulp.dest('dist'))
});
